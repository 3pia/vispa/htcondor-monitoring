# HTCondor Monitoring

# Setup conda environment:

(First on VISPA: `software conda`)
```
conda env create --name htcondor --file=environments.yaml
conda activate htcondor
```

# Run monitoring
Monitor all jobs:
```bash
./htc
```


Monitor jobs from a specific user:
```
./htc -u pfackeldey
```

More options can be found by appending `--help`.
