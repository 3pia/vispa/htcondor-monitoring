# coding: utf-8

"""
Basic pyhton class abstractions of logical HTCondor classes.
"""

__all__ = ["CurrentUser", "HTCondorCommand", "HTCondorJob", "HTCondorJobs", "HTCondorSummary"]


import json
import getpass
import socket
from dataclasses import dataclass, make_dataclass
from typing import List, Type, Dict
from subprocess import check_output

from rich.console import Console, ConsoleOptions, RenderResult
from rich.table import Table


@dataclass
class CurrentUser:
    name: str = str(getpass.getuser())
    host: str = str(socket.gethostname())

    def __console__(self, console: Console, options: ConsoleOptions) -> RenderResult:
        yield f"[b]User[/b]: {self.name}"
        yield f"[b]Host[/b]: {self.host}"


@dataclass
class HTCondorJob:
    owner: str
    id: str
    run_time: str
    # requested
    rcpus: str
    rmem: str
    rgpumem: str
    rgpus: str
    # used
    umem: str
    ugpumem: str
    # job host
    host: str

    def __post_init__(self):
        self.run_time = "{:.1f}".format(float(self.run_time) / 60.0)
        self.umem = "{:.1f}".format(float(self.umem) / 1024)


@dataclass
class HTCondorJobs:
    jobs: List[Type[HTCondorJob]]

    def req_color(self, used: str, req: str) -> str:
        u = float(used)
        r = float(req)
        if u <= 0.9 * r:
            return f"[green]{u}"
        elif (u > 0.9 * r) and (u <= r):
            return f"[orange]{u}"
        else:
            return f"[b][red]{u}"

    def __console__(self, console: Console, options: ConsoleOptions) -> RenderResult:
        yield f"[b]Statistics[/b]:"
        yield f"[b]Number of jobs: {str(len(self.jobs))}[/b]"
        yield f"\n[green]Resources are below 90% of request"
        yield f"[dark_orange]Resources are between 90% and 100% of request"
        yield f"[b][red]Resources above 100% of request"
        job_table = Table()
        job_table.add_column("User", style="cyan", justify="right")
        job_table.add_column("Job ID", style="cyan", justify="right")
        job_table.add_column("Job host", style="cyan", justify="right")
        job_table.add_column("Job runtime in minutes", style="dim", justify="right")
        job_table.add_column("Requested CPUs", style="dim", justify="right")
        job_table.add_column("Requested Memory in MB", style="dim", justify="right")
        job_table.add_column("Requested GPUs", style="dim", justify="right")
        job_table.add_column("Requested GPU Memory in MB", style="dim", justify="right")
        job_table.add_column("Used Memory in MB", justify="right")
        job_table.add_column("Used GPU Memory in MB", justify="right")
        for j in self.jobs:
            job_table.add_row(
                j.owner,
                j.id,
                j.host,
                j.run_time,
                j.rcpus,
                j.rmem,
                j.rgpus,
                j.rgpumem,
                self.req_color(j.umem, j.rmem),
                self.req_color(j.ugpumem, j.rgpumem),
            )
        yield job_table


@dataclass
class HTCondorSummary:
    jobs: List[Type[HTCondorJob]]

    def __post_init__(self):
        # accumulate across users
        AccJob = make_dataclass(
            "AccJob",
            [
                ("owner", str),
                ("njobs", str),
                ("rcpus", str),
                ("rmem", str),
                ("rgpumem", str),
                ("rgpus", str),
                ("umem", str),
                ("ugpumem", str),
            ],
        )
        self.acc_jobs = []
        users = set(j.owner for j in self.jobs)
        for user in users:
            user_jobs = list(filter(lambda x: x.owner == user, self.jobs))
            acc_rcpus = self.acc([j.rcpus for j in user_jobs])
            acc_rmem = self.acc([j.rmem for j in user_jobs])
            acc_rgpus = self.acc([j.rgpus for j in user_jobs])
            acc_rgpumem = self.acc([j.rgpumem for j in user_jobs])
            acc_umem = self.acc([j.umem for j in user_jobs])
            acc_ugpumem = self.acc([j.ugpumem for j in user_jobs])
            self.acc_jobs.append(
                AccJob(
                    owner=user,
                    njobs=str(len(user_jobs)),
                    rcpus=acc_rcpus,
                    rmem=self.to_gb(acc_rmem),
                    rgpus=acc_rgpus,
                    rgpumem=self.to_gb(acc_rgpumem),
                    umem=self.to_gb(acc_umem),
                    ugpumem=self.to_gb(acc_ugpumem),
                )
            )

    def acc(self, v: List[str]) -> str:
        return str(sum(map(float, v)))

    def to_gb(self, v: str) -> str:
        return "{:.1f}".format(float(v) / 1024)

    def req_color(self, used: str, req: str) -> str:
        u = float(used)
        r = float(req)
        if u <= 0.9 * r:
            return f"[green]{u}"
        elif (u > 0.9 * r) and (u <= r):
            return f"[orange]{u}"
        else:
            return f"[b][red]{u}"

    def __console__(self, console: Console, options: ConsoleOptions) -> RenderResult:
        yield "[b]Summary statistics for all users:[/b]"
        yield "(Values represent the sum across all jobs)"
        job_table = Table()
        job_table.add_column("User", style="cyan", justify="right")
        job_table.add_column("Number of jobs", style="cyan", justify="right")
        job_table.add_column("Requested CPUs", style="dim", justify="right")
        job_table.add_column("Requested Memory in GB", style="dim", justify="right")
        job_table.add_column("Requested GPUs", style="dim", justify="right")
        job_table.add_column("Requested GPU Memory in GB", style="dim", justify="right")
        job_table.add_column("Used Memory in GB", justify="right")
        job_table.add_column("Used GPU Memory in GB", justify="right")
        for j in self.acc_jobs:
            job_table.add_row(
                j.owner,
                j.njobs,
                j.rcpus,
                j.rmem,
                j.rgpus,
                j.rgpumem,
                self.req_color(j.umem, j.rmem),
                self.req_color(j.ugpumem, j.rgpumem),
            )
        yield job_table


class HTCondorCommand(object):
    @classmethod
    def condor_q(cls) -> List[Dict[str, str]]:
        cmd = [
            'COLS="Owner GlobalJobId RequestCPUs RequestMemory RequestGpuMemory RequestGPUs '
            'StatsLifetimeStarter ResidentSetSize GPUsMemoryUsage RemoteHost"'
            ' && ( condor_q -global -allusers -run "$@" -af $COLS -json )'
        ]
        p = check_output(cmd, shell=True)
        out = json.loads(p)
        return out
