# coding: utf-8

import time
from datetime import datetime
import subprocess

import click
import rich

from htcmon import (
    CurrentUser,
    HTCondorCommand,
    HTCondorJob,
    HTCondorJobs,
    HTCondorSummary,
)


@click.command()
@click.option("-user", "-u", default="all", help="Show HTCondor statistics for a user")
@click.option(
    "--poll", "-p", default=10, help="Poll interval to refresh HTCondor statistics"
)
def run(user: str, poll: int) -> None:
    console = rich.console.Console(log_time=False)
    while True:
        jobs = []
        for info in HTCondorCommand.condor_q():
            for k in [
                "RequestCPUs",
                "RequestMemory",
                "RequestGpuMemory",
                "RequestGPUs",
                "ResidentSetSize",
                "GPUsMemoryUsage",
                "StatsLifetimeStarter",
            ]:
                info.setdefault(k, "0")
            if "Dask" in str(info["RequestMemory"]):
                info["RequestMemory"] = None
            jobs.append(
                HTCondorJob(
                    owner=str(info["Owner"]),
                    id=str(info["GlobalJobId"].split("#")[1]),
                    run_time=str(info["StatsLifetimeStarter"]),
                    rcpus=str(int(info["RequestCPUs"] or 0)),
                    rmem=str(int(info["RequestMemory"] or 0)),
                    rgpumem=str(int(info["RequestGpuMemory"] or 0)),
                    rgpus=str(int(info["RequestGPUs"] or 0)),
                    umem=str(int(info["ResidentSetSize"] or 0)),
                    ugpumem=str(int(info["GPUsMemoryUsage"] or 0)),
                    host=str(info["GlobalJobId"].split(".physik")[0]),
                )
            )
        if user == "all":
            statistics = HTCondorJobs(jobs=jobs)
        else:
            user_jobs = list(filter(lambda x: x.owner == user, jobs))
            statistics = HTCondorJobs(jobs=user_jobs)

        subprocess.call("clear")
        console.log(CurrentUser())
        console.print(
            "[bold]Time:[/bold]", datetime.now().strftime("%d.%m.%Y - %H:%M:%S")
        )
        console.log(statistics)
        summary_statistics = HTCondorSummary(jobs=jobs)
        console.log(summary_statistics)
        time.sleep(poll)
